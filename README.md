xivo-provd
==========

Running unit tests
------------------

```
apt-get install python-dev libffi-dev libssl-dev
pip install tox
tox --recreate -e py37
```
