#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from distutils.core import setup

setup(
    name='provisioning',
    version='0.1',
    description='XIVO provisioning daemon',
    maintainer='Avencall',
    maintainer_email='dev@xivo.solutions',
    url='http://xivo.solutions/',
    license='GPLv3',

    packages=['provd',
              'provd.devices',
              'provd.persist',
              'provd.rest',
              'provd.rest.server',
              'provd.servers',
              'provd.servers.tftp',
              'twisted',
              'twisted.plugins'],
    package_data={'provd': ['tzinform/tzdatax'],
                  'twisted': ['.noinit'],
                  'twisted.plugins': ['provd_plugins.py', '.noinit']}
)
